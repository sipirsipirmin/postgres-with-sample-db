FROM postgres

RUN apt-get update && apt-get install wget -y 

RUN wget https://gitlab.com/sipirsipirmin/todevops/raw/master/initialdb.sql
RUN sleep 1
ADD ./initialdb.sql /docker-entrypoint-initdb.d
